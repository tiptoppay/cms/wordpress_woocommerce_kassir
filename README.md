#  Kassir TipTop Pay модуль для WordPress WooCommerce
Модуль позволяет интегрировать онлайн-кассу [TipTop Pay](https://tiptoppay.kz) в интернет-магазин на WordPress WooCommerce. 
Для корректной работы модуля необходима регистрация в сервисе.
Порядок регистрации описан в [документации TipTop Pay](https://tiptoppay.kz/#connect).

### Возможности:  
    
* Поддержка онлайн-касс;
* Автоматическая отправка чеков прихода;
* Отправка чеков возврата;
* Отдельный параметр НДС для доставки;
* Теги способа и предмета расчета;
* Отправка чеков на email клиента;
* Отправка чеков по SMS.

### Совместимость:  
WordPress 4.9.7 и выше;  
WooCommerce 3.4.4 и выше.  

_Если вы используете платежный модуль TipTop Pay совместно с данным модулем, то убедитесь, что в платежном модуле отключена отправка чеков через онлайн-кассу, во избежание дублирования кассовых чеков._

## Установка модуля
1) Скопируйте содержимое в `/wp-content/plugins`
2) В `/wp-admin/plugins.php` активируйте `WooCommerce Kassir TipTop Pay Gateway`
3) `/wp-admin/admin.php?page=wc-settings&tab=checkout` в столбце `Включить` укажите `нет`  
_Это необходимо, чтобы приложение не отображалось в корзине как способ оплаты_   

## Настройка модуля
Для настройки модуля перейдите в раздел `WooCommerce` -> `Платежи` -> `Kassir TipTop Pay`  

Укажите следующие настройки:
* **Статус для печати чека прихода** - Выполнен (Если не предусматривается другой функционал);
* **Статус для печати чека возврата** - Отменен (Если не предусматривается другой функционал);
* **Public_id** - Public id сайта из личного кабинета TipTop Pay;
* **Password for API** - API Secret из личного кабинета TipTop Pay;
* **ИИН/БИН** - ИИН/БИН вашей организации или ИП, на который зарегистрирована касса;
* **Место осуществления расчёта** - Укажите сайт точки продаж для печати в чеке;
* **Ставка НДС** - Укажите ставку НДС товаров;
* **Ставка НДС для доставки** - Укажите ставку НДС службы доставки;
* **Система налогообложения организации** - Тип системы налогообложения;
* **Способ расчета** - признак способа расчета;
* **Предмет расчета** - признак предмета расчета;
* **Статус доставки** - Отдельный статус доставки необходим при формировании двух чеков: один чек - при поступлении денег от покупателя, второй при отгрузке товара. Отправка второго чека возможна при следующих способах расчета: `Предоплата`, `Предоплата 100%`, `Аванс`.

### Настройка вебхуков
Для получения URL адреса копии отправленного онлайн-чека в комментариях к заказу необходимо в [личном кабинете](https://merchant.tiptoppay.kz) TipTop Pay в настройках вашего сайта вставьте следующий URL для коректной работы модуля:

* **Receipt**  
`https://domain.kz/wc-api/WC_tiptoppaykassir?action=receipt`  

Где **domain.kz** - адрес сайта.

### Frequently Asked Questions

#### Какой порядок подключения? 

Для подключения к сервису аренды онлайн-касс TipTop Pay, необходимо выполнить следующие действия:
* Оставить заявку на [сайте](https://tiptoppay.kz/#connect).
* Получить ответ от персонального менеджера. Он будет сопровождать на всех этапах.
* Ознакомиться с преимуществами работы и списком необходимых документов.
* Договориться о коммерческих условиях с персональным менеджером.
* Получить доступ в личный кабинет. Он необходим для подписания договора и дальнейшей работы со всеми инструментариями TipTop Pay.
* Выполнить техническую интеграцию сайта.
* Провести тестовые чеки. После успешных тестов — сообщить об этом менеджеру, который переведет кассу в боевой режим. 

### Changelog

**1.0.0**
- Публикация модуля